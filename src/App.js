import { Component } from "react";
import Header from "./components/Header";
import Tablero from "./components/Tablero";

import baraja from "./utils/baraja";

const NUMERO_CARTAS = 4;

const initialState = {
  baraja: baraja(NUMERO_CARTAS),
  parejaSeleccionada: [], // solo colocamos 2 cartas
  estamosComparando: false,
  numeroDeIntentos: 0,
  adivinada : 0
};

export default class App extends Component {
  state = {
    ...initialState,
  };

  seleccinarCatarta = (carta) => {
    console.log("la info de la carta es:", carta);

    /**reglas de validación
     * si estamos comparando no volteamos otras cartas
     * si la carta fue adivinada no la volvemos a volterar
     * si la carta seleccionada, esta en el arreglo comparador tampoco la volteamos
     */

    if (
      this.state.estamosComparando ||
      carta.fueAdivinada ||
      this.state.parejaSeleccionada.indexOf(carta) > -1
    ) {
      return;
    }

    const parejaSeleccionada = [...this.state.parejaSeleccionada, carta];
    this.setState({ ...this.state, parejaSeleccionada: parejaSeleccionada });

    if (parejaSeleccionada.length === 2) {
      //creamos una funcion para comparar las 2 cartas
      this.compararPareja(parejaSeleccionada);
      console.log(this.state.parejaSeleccionada);
    }
  };

  compararPareja = (arregloComparador) => {
    this.setState((prevState) => ({ ...prevState, estamosComparando: true }));

    setTimeout(() => {
      const [primeraCarta, segundaCarta] = arregloComparador;
      let baraja = this.state.baraja;

      if (primeraCarta.icono === segundaCarta.icono) {
        baraja = baraja.map((carta) => {
          if (carta.icono !== primeraCarta.icono) {
            return carta;
          } else {
            this.state.adivinada = this.state.adivinada + 1;
            return { ...carta, fueAdivinada: true };
          }
        });
      }
      if(this.state.adivinada === NUMERO_CARTAS) alert(`FELICIDADES, GANASTE A LOS ${this.state.numeroDeIntentos + 1} INTENTOS`)
      this.setState({
        baraja: baraja,
        parejaSeleccionada: [],
        estamosComparando: false,
        numeroDeIntentos: this.state.numeroDeIntentos + 1,
      });
    }, 1000);
  };

  resetearPartida = () => {
    this.setState({ ...initialState, baraja: baraja() });
  };

  render() {
    return (
      <div className="App">
        <Header
          intentos={this.state.numeroDeIntentos}
          resetear={this.resetearPartida}
        />
        <Tablero
          baraja={this.state.baraja}
          parejaSeleccionada={this.state.parejaSeleccionada}
          seleccinarCatarta={this.seleccinarCatarta}
        />
      </div>
    );
  }
}
