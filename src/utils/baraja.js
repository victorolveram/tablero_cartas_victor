import shuffle from "lodash.shuffle";
import { listado } from "./fa";


const baraja = (NUMERO_CARTAS) => {
  let cartas = [];
  const iconos = listado();

  while (cartas.length < NUMERO_CARTAS) {
    const indice = Math.floor(Math.random() * iconos.length);

    const carta = {
      icono: iconos.splice(indice, 1)[0], // [ "fa-android"]
      fueAdivinada: false,
    };
    cartas.push(carta);
    cartas.push({ ...carta });
  }

  return shuffle(cartas);
};

export default baraja;
