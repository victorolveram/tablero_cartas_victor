import React from "react";

const Header = ({intentos, resetear}) => {

  const reset = (e) => {
    e.preventDefault();
    resetear();
  }

  return (
    <nav
      className="navbar navbar-expand-lg navbar-dark bg-dark"
      style={{ padding: "0 1em" }}
    >
      <div className="container-fluid">
        <p className="navbar-brand">Juego de cartas</p>
        <div className="collapse navbar-collapse">
          <h3 className="m-auto text-white">Intentos: {intentos}</h3>
          <form className="d-flex">
            <button onClick={reset} className="btn btn-light">Reiniciar</button>
          </form>
        </div>
      </div>
    </nav>
  );
};

export default Header;
