import React from "react";
import Carta from "./Carta";
const Tablero = ({ baraja, seleccinarCatarta, parejaSeleccionada }) => {
  // [C1, C2]
  return (
    <div
      className="d-flex justify-content-between flex-wrap"
      style={{ width: "46em", margin: "0 auto" }}
    >
      {baraja.map((carta, index) => {
        const estaSiendoCOmparada = parejaSeleccionada.indexOf(carta) > -1;
        return (
          <Carta
            key={index}
            info={carta}
            seleccinarCatarta={() => seleccinarCatarta(carta)}
            estaSiendoCOmparada={estaSiendoCOmparada}
          />
        );
      })}
    </div>
  );
};

export default Tablero;
