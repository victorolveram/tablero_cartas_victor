import React, { Component } from "react";
import { stilos } from "../style/carta";

import ReactCardFlip from "react-card-flip";

export default class Carta extends Component {
  render() {
    const { icono, fueAdivinada } = this.props.info;
    const { seleccinarCatarta, estaSiendoCOmparada } = this.props;

    const mostrarCarta = fueAdivinada || estaSiendoCOmparada;

    return (
      <div className="card my-2" style={stilos.carta}>
        <ReactCardFlip isFlipped={mostrarCarta}>
          <div
            className="card-body"
            style={stilos.backCard}
            onClick={seleccinarCatarta}
          >
            <p className="card-text">l</p>
          </div>
          <div className="card-body" style={stilos.frontCard}>
            <p className="card-text">
              <i className={`fa ${icono}`} />
            </p>
          </div>
        </ReactCardFlip>
      </div>
    );
  }
}
