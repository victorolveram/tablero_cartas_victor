const common = {
  margin: "0 auto",
  width: "125px",
  height: "125px",
  borderRadius: "5px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  cursor: "pointer",
  fontSize: "2em",
};
export const stilos = {
  carta: {
    margin: "0 auto",
    width: "125px",
    height: "125px",
  },

  backCard: {
    ...common,
    backgroundColor: "#ffb300",
  },
  frontCard: {
    ...common,
    backgroundColor: "#03dcf4",
  },
};
